[revolt]: https://github.com/revoltchat

# [Revolt][revolt] messages and embeds bugs

## Bugs

- A no-break space has to be added for empty lines in a multi-line quite to preserve them (or another invisible character except whitespace).
- Empty lines in code blocks have a no-break space character in them. This behavior can break some compilers/interpreters.
- If a backslash is followed by another backslash or some other symbols, it is evaluated. This breaks some kaomojis, such as shrug &macr;\\\_(&#12484;)\_/&macr;.
- If you declare an unordered or ordered list, the rest of it will be treated as either (1) the same list when creating another, or (2) the content of the last item. A workaround is to use a page break (e.g. `***`) which will mark an end of a list, while it would be rendered as an empty line.
- Images of media previews are too big in relation to the text.
- In code blocks, after a sequence of one or multiple lines starting with `>`, an empty line is added.
- In ordered lists, if the content is changed so that index start number is decreased, the indexing is shifted by -1.
- Page breaks do not work as intended, but are parsed (the characters are removed), effectively generating empty lines.
- Some markdown, such as headers without text, is evaluated. It shouldn't be evaluated.
- Using non-escaped (by a backslash) `<` followed by some text, `>`, and nothing else in a quote results in reduction of space between the visual quote block and the quote.
- When replying to messages, a single message or multiple messages being replied to are parsed as markdown, but as if all line breaks (newlines) have been removed, which creates a mix-up.

## Conclusions

- Markdown mode should be optional.
- A visual mode for Markdown mode could be added to simplify text editing for beginners.
