# Human usage of desktop and mobile devices

**Topic**: Several threats related to **human usage of desktop/mobile devices**

**Task**: Categorize and simplify knowledge on the topic, make it easily accessible and unambigious. Solve the task using only open, free, libre and top-notch quality solutions.

**Content**:  
There are several threads related to human usage of desktop/mobile devices:
- Blue light (vision)
- Computer addiction (psychology)
- Dehydration (general health)
- Doxing/data breaches (psychology, security)
- Eye strain (vision)
- Improper use of speakers/headphones/earphones, i.e. listening at too high a volume (hearing)
- Lack of fresh air (general health)
- Lack of muscle activity (general health)
- Sleep deprivation (general health)

Furthermore, many people are unaware of time-based brain optimization techniques, such as the Pomodoro technique.

**Possible solutions**:
- Create infographics in SVG format and distribute those freely under Creative Commons Zero 1.0 Universal.
