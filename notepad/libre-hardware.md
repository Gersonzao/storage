# Libre hardware

> **Note**
> by Gersonzao

- the Raspberry Pi 5 is [capable of running GameCube and PSP games smoothly](https://inv.n8pjl.ca/watch?v=wSN1L2XyyUY)
- RISC-V CPU towers like the [Milk-V Pioneer](https://milkv.io/pioneer) are very power efficient machines and slowly getting more popular though unfortunately [not very powerful](https://inv.n8pjl.ca/watch?v=vaMxTSm53UU)... I believe the Milk-V Pioneer is currently the most powerful one
- Pine64 makes ARM- and RISC-V-based laptops: https://pine64.org/devices/
- Purism has [disabled](https://puri.sm/learn/intel-me/) and [neutralised](https://puri.sm/learn/intel-me/) ME and AMT on the [Librem Mini](https://puri.sm/products/librem-mini/) mini computer [Librem 14](https://puri.sm/products/librem-14/) laptop and [second-generation laptops](https://puri.sm/posts/deep-dive-into-intel-me-disablement/), and are helping on reverse engineering it
- the FSF has an endorsed hardware (components and full machines) [page](https://ryf.fsf.org)
- 'h-node.org is a repository that contains information about how well particular hardware works with free software.': https://www.gnu.org/help/help
