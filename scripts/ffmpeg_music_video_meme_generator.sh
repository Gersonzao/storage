#!/bin/sh

error() {
	echo "$1 doesn't exist."
	exit 1
}

payload() {
	mkdir ./build > /dev/null 2>&1
	ffmpeg -loop 1 -i "$1" -i "$2" -map_metadata -1 -c:a libmp3lame -ar 22050 -ac 1 -b:a 64k -c:v libx264 -r 25 -crf 45 -movflags +faststart -shortest ./build/output.mp4
	exit 1
}

if [ -z "$1" ] || [ -z "$2" ]; then
	echo "$0 <image> <audio>"
	exit 0
fi

# check if the specified files exist
if [ ! -f "$1" ]; then
	error "$1"
	exit 1
fi
if [ ! -f "$2" ]; then
	error "$2"
	exit 1
fi

payload "$1" "$2"
exit 0
