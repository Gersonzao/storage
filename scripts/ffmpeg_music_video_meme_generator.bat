@echo off

goto :main

:error
echo %1 doesn't exist.
exit /b 1

:payload
mkdir build  2> nul
ffmpeg -loop 1 -i %1 -i %2 -map_metadata -1 -c:a libmp3lame -ar 22050 -ac 1 -b:a 64k -c:v libx264 -r 25 -crf 45 -movflags +faststart -shortest build\output.mp4
exit /b 1

:main
set can_run=1
if "%1" equ "" set can_run=0
if "%2" equ "" set can_run=0

if "%can_run%" equ "0" (
	set "basename=%~n0%~x0"
	echo:%basename% ^<image^> ^<audio^>
	goto :EOF
)

if not exist "%1" ( call :error %1 || goto :EOF )
if not exist "%2" ( call :error %2 || goto :EOF )
call :payload %1 %2

goto :EOF

:EOF
