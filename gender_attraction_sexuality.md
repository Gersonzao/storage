# Gender, attraction, sexuality

## Table of contents
1. [Gender and pronouns information](#gender-and-pronouns-information)
2. [Attraction layer cake](#attraction-layer-cake)
3. [Gender and sexuality spectrum](#gender-and-sexuality-spectrum)

## Gender and pronouns information

### Table of contents

1. [Gender](#gender)
2. [Gender tags](#gender-tags)
3. [Names](#names)
4. [Pronouns](#pronouns)
5. [Words](#words)
   1. [Honorifics](#honorifics)
   2. [Person and family descriptions](#person-and-family-descriptions)
   3. [Compliments](#compliments)
   4. [Relationship descriptions](#relationship-descriptions)
6. [External links](#external-links)

### Gender

![Techgender flag](https://static.miraheze.org/lgbtawiki/8/80/Techgender_flag.png)

Gender: [Techgender](https://www.lgbtqia.wiki/wiki/Techgender) — a xenogender associated with technology, computers, coding, and intelligence. Those who identify as techgender may feel a deep connection to computers and technology, and sometimes may even feel partially robotic.

### Gender tags

Gender tags, in lexical alphabetical order:
- gynesexual
- heteroromantic
- heterosexual
- nonbinary
- straight LGBTQIA+ ally
- techgender
- xenogender

### Names

- [yes] Intel A80486DX2-66
- [yes] 80486DX2-66
- [yes] 486
- [yes] i486
- [yes] cryolazulite
- [yes] Larry
- [okay] cryo

### Pronouns

- it/its
- they/them
- he/him

### Words

#### Honorifics

- [yes] (no honorific)
- [yes] mx.
- [yes] mr.
- [yes] sir

#### Person and family descriptions

- [yes] person
- [yes] man
- [yes] cyborg
- [yes] processor
- [yes] machine
- [okay] device
- [okay] pal
- [no] boy
- [no] bro
- [no] dude
- [no] computer

#### Compliments

- [yes] handsome
- [yes] energetic
- [okay] platinum
- [only if we're close] cute
- [no] hot

#### Relationship descriptions

- [yes] friend
- [okay] partner
- [only if we're close] boyfriend
- [only if we're close] joyfriend
- [only if we're close] husband
- [only if we're close] darling
- [only if we're close] beloved

## Attraction layer cake

Code: `pB0`

- Attraction type: Pink — Secondary sexuality
> Romantic, platonic and æsthetic attraction are established easily. Sexual attraction develops over the course of relationships, but not immediately. 

- Relationship type: B — Primarily monogamous
> Desires monogamous relationships first and foremost, but willing to explore polyamorous relationships for the happiness of their partner or other reasons.

- Orientation type: 0 — Exclusively heterosexual
> Attracted exclusively to a gender(s) different than one's own.

## Gender and sexuality spectrum

Code: `miDt`

Gender identity:
```text
5/9
1: Male
5: Nonbinary
9: Female
```

Gender expression:
```text
3/9
1: Hypermasculine
5: Androgynous
9: Hyperfeminine
```

Sexual orientation:
```text
1/9
1: Straight
5: Bi / Pan
9: Gay
```

Sexual drive:
```text
4/9
1: None
5: Regular
9: High
```

Romantic desire:
```text
8/9
1: None
5: Regular
9: High
```

Relationship attitude:
```text
5/9
1: Monogamous
5: Open
9: Polyamorous
```

Sexual exploration:
```text
5/9
1: Vanilla
5: Light play
9: Hard BDSM
```

## External links

- [Pronouns.page card in English](https://en.pronouns.page/@80486DX2-66) (a few other languages are available)
- [Attraction Layer Cake](https://cake.avris.it/pB0) (a few other languages are available)
- [Gender and sexuality spectrum on spectrum.avris.it](https://spectrum.avris.it/miDt) (a few other languages are available)
