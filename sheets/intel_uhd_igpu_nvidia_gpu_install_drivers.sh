#!/bin/sh

# original by Furret <https://furret.net>; Discord: Furret#3961
# minimized and changed by i486

echo "This script installs drivers for the Intel UHD iGPU and the nVidia GPU,"
echo "after this you have to manually configure optimus-manager since we cannot"
echo "build in root (this is chroot right?)"
echo "After this you'll have to restart, and boom, your machine will be"
echo "configured for optimus"
echo
echo "Uses Pacman to install packages: neofetch, xf86-video-intel, nvidia,"
echo "nvidia-settings, nvidia-utils, mesa, flatpak, bbswitch, haxe, discord,"
echo "git"
echo
echo "Press any key to continue if you're ready or press Ctrl+C to abort..."
read -s -n 1
sudo pacman -S --noconfirm neofetch
neofetch
read -s -n 1
echo "finishing stuff"
sudo pacman -S --noconfirm xf86-video-intel nvidia nvidia-settings nvidia-utils mesa flatpak bbswitch haxe discord git
echo "done!"
