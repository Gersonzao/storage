[86box]: https://86box.net

# Ideas for [86Box][86box]

- Mouse integration support [like VirtualBox](https://www.virtualbox.org/wiki/MouseInput)
- Text selection, copying, pasting in text mode, assuming the text is in ASCII or a mutually supported encoding

